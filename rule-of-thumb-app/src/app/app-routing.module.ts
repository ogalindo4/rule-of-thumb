import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: '', redirectTo:'rule-of-thumb', pathMatch:'full'},
  {
    path:'rule-of-thumb',loadChildren:()=>import('./shared/layout/layout.module').then(m=>m.LayoutModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
