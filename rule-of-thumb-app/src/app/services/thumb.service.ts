import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ThumbService {

  urlInfoPersons = './../../assets/data/person.json';
  constructor(private http: HttpClient, private router: Router) { }

  consultInfoPersons(): Observable<any> {
    console.log('servicio person')
    const url = this.urlInfoPersons;
    return this.http.get(url);
  }
  
}
