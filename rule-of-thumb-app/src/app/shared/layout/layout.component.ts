import { Component, OnInit } from '@angular/core';
import { ThumbService } from 'src/app/services/thumb.service';


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  constructor(private servicioThumb: ThumbService) { }

  ngOnInit(){
    this.consultsPersons();
  }

  consultsPersons(){
    this.servicioThumb.consultInfoPersons()
    .subscribe((result) => {
      localStorage.setItem('personInfo', JSON.stringify(result.person));
      console.log(result,'result');
      // for (let i = 0; i < result.length; i++ ) {
      //   this.informationPersons.push( result[i] );
      //   console.log(this.informationPersons, 'votes info person');
      // }
    } );
  }

  

}
