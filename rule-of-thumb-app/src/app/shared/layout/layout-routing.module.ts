import { NgModule } from '@angular/core';
import { LayoutComponent } from './layout.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: LayoutComponent, children: [
      {​​​​​​​​
        path: 'votes', loadChildren: () =>
        import('./../../components/votes/votes.module').then(m => m.VotesModule)
      },
      {​​​​​​​​
        path: 'Past-Trials', loadChildren: () =>
        import('./../../components/past-trials/past-trials.module').then(m => m.PastTrialsModule)
      },
      {​​​​​​​​
        path: 'How-It-Works', loadChildren: () =>
        import('./../../components/how-it-works/how-it-works.module').then(m => m.HowItWorksModule)
      },
      {​​​​​​​​
        path: 'LogIn-SignUp', loadChildren: () =>
        import('./../../components/login-signup/login-signup.module').then(m => m.LoginSignupModule)
      }

    ]
  }
]    

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
