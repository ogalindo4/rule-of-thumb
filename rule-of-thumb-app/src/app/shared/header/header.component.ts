import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  elementClicked: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  onClickOption(elemento: string){
    this.elementClicked = elemento.trim();
    console.log(this.elementClicked, 'element');
  }

  isSelected(item: string) {
    if (item.trim() === this.elementClicked.trim()) {
      console.log('selected true');
      return true;
    } else {
      console.log('selected false');
      return false;
    }
  }

}
