import { NgModule } from '@angular/core';
import { PastTrialsComponent } from './past-trials.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: PastTrialsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PastTrialsRoutingModule { }
