import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PastTrialsRoutingModule } from './past-trials-routing.module';
import { PastTrialsComponent } from './past-trials.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    PastTrialsComponent
  ],
  imports: [
    CommonModule,
    PastTrialsRoutingModule,
    NgbModule
  ]
})

export class PastTrialsModule { }
