import { Component, OnInit } from '@angular/core';
import { ThumbService } from 'src/app/services/thumb.service';

export class Person {
  against: number;
  favor: number;
  lastVote: number;
  name: string;
  sector: string;
  time: number;
  votes: number;
  perAgainst: number;
  perFavor: number;
  result: number;
  voteAgain: boolean;
}

@Component({
  selector: 'app-votes',
  templateUrl: './votes.component.html',
  styleUrls: ['./votes.component.scss']
})
export class VotesComponent implements OnInit {
  
  informationPersons: Person[];

  // persons: []
  constructor(private servicioThumb: ThumbService) { }

  ngOnInit(): void {
    this.informationPersons = JSON.parse(localStorage.getItem('personInfo'));
  }

  vote(person: number, vote: number){
    console.log(person, 'evento');
    console.log(vote, 'vote');
    this.informationPersons[person].lastVote = vote;
    if (vote === 1) {
      console.log('favor');
      this.informationPersons[person].favor = this.informationPersons[person].favor + 1;
    }else {
      console.log('contra');
      this.informationPersons[person].against = this.informationPersons[person].against + 1;
    }
    this.informationPersons[person].votes = this.informationPersons[person].votes + 1;
    this.informationPersons[person].perAgainst = (100 * this.informationPersons[person].against )/this.informationPersons[person].votes;
    this.informationPersons[person].perFavor = (100 * this.informationPersons[person].favor )/this.informationPersons[person].votes;
    if (this.informationPersons[person].perAgainst > this.informationPersons[person].perFavor) {
      this.informationPersons[person].result = 0;
     } else {
      this.informationPersons[person].result = 1;
     }
     this.informationPersons[person].voteAgain = true;
    console.log(this.informationPersons[person], 'person');
    localStorage.setItem('personInfo', JSON.stringify(this.informationPersons));
  }

  voteAga(person: number) {
    this.informationPersons[person].voteAgain = false;
    localStorage.setItem('personInfo', JSON.stringify(this.informationPersons));
  }

  

}
