import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HowItWorksComponent } from './how-it-works.component';
import { HowItWorksRoutingModule } from './how-it-works-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    HowItWorksComponent
  ],
  imports: [
    CommonModule,
    HowItWorksRoutingModule,
    NgbModule
  ]
})

export class HowItWorksModule { }
