import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginSignupRoutingModule } from './login-signup-routing.module';
import { LoginSignupComponent } from './login-signup.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    LoginSignupComponent
  ],
  imports: [
    CommonModule,
    LoginSignupRoutingModule,
    NgbModule
  ]
})

export class LoginSignupModule { }
